package com.billing.gayatri.ui.activities;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.billing.gayatri.R;
import com.billing.gayatri.database.DatabaseHelper;
import com.billing.gayatri.helpers.tableview.TableViewModel;
import com.billing.gayatri.helpers.tableview.adapters.TableViewAdapter;
import com.billing.gayatri.helpers.tableview.interfaces.TableViewListener;
import com.billing.gayatri.models.BillingOfflineModel;
import com.billing.gayatri.utils.PermissionUtils;
import com.evrencoskun.tableview.TableView;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class BillActivity extends AppCompatActivity {
    private static final String TAG = BillActivity.class.getName();
    SQLiteDatabase database;
    DatabaseHelper helper;

    TableView mTableView;
    Button scrollFirst, scrollLast;

    String filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill);
        filter = getIntent().getStringExtra("filter");
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        helper = databaseHelper;
        database = databaseHelper.getWritableDatabase();
        initIds();
        saveToList();
        initializeTableView();
    }

    private void initIds() {
        mTableView = findViewById(R.id.mTableView);
        scrollFirst = findViewById(R.id.scrollFirst);
        scrollLast = findViewById(R.id.scrollLast);

        scrollFirst.setOnClickListener(v->{
            mTableView.getCellRecyclerView().scrollToPosition(0);
            mTableView.getRowHeaderRecyclerView().scrollToPosition(0);
        });

        scrollLast.setOnClickListener(v->{
            mTableView.getCellRecyclerView().scrollToPosition(billingList.size() - 1);
            mTableView.getRowHeaderRecyclerView().scrollToPosition(billingList.size() - 1);
        });
    }

    private void initializeTableView() {
        // Create TableView View model class  to group view models of TableView
        TableViewModel tableViewModel = new TableViewModel();

        // Create TableView Adapter
        TableViewAdapter tableViewAdapter = new TableViewAdapter(tableViewModel);

        mTableView.setAdapter(tableViewAdapter);
        TableViewListener tableViewListener = new TableViewListener(mTableView);
        mTableView.setTableViewListener(tableViewListener);

        // Create an instance of a Filter and pass the TableView.
        //mTableFilter = new Filter(mTableView);

        // Load the dummy data to the TableView
        if (tableViewModel.getCellList().size() > 0)
            tableViewAdapter.setAllItems(tableViewModel.getColumnHeaderList(), tableViewModel
                    .getRowHeaderList(), tableViewModel.getCellList());

    }

    private void createPDF() throws FileNotFoundException {
        String path = new File(getExternalFilesDir(null).getPath() + "/" + getString(R.string.app_name)).getPath();
        Log.d("TAG", "generatePdf: " + "Folder created");
        File file = new File(path);
        File pdfPath = new File(file, "DemoPDF.pdf");
        if (!file.exists())
            if (file.mkdir())
                Log.d("TAG", "generatePdf: " + "Folder created");
            else Log.d("TAG", "generatePdf: " + "Error creating Folder");
        else Log.d("TAG", "generatePdf: " + "Folder exists");

        PdfWriter pdfWriter = new PdfWriter(pdfPath.getPath());
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);

        float[] columnWidth = {100f, 100f, 100f, 100f, 100f, 100f, 100f, 100f, 100f};
        Table table = new Table(columnWidth);

        Cell cell_11 = new Cell();
        table.addCell(new Cell().add("Bill No."));
        table.addCell(new Cell().add("Days"));
        table.addCell(new Cell().add("Bill Date"));
        table.addCell(new Cell().add("Bill Amt"));
        table.addCell(new Cell().add("Vou. No."));
        table.addCell(new Cell().add("Vou. Date."));
        table.addCell(new Cell().add("Note"));
        table.addCell(new Cell().add("Vou. Amount"));
        table.addCell(new Cell().add("Balance"));

        for (int i = 0; i < 50; i++) {
            table.addCell(new Cell().add(billingList.get(i).getBillno()));
            table.addCell(new Cell().add(billingList.get(i).getDays() + ""));
            table.addCell(new Cell().add(billingList.get(i).getBilldate()));
            table.addCell(new Cell().add(billingList.get(i).getBillamt() + ""));
            table.addCell(new Cell().add(billingList.get(i).getReceiptno() + ""));
            table.addCell(new Cell().add(billingList.get(i).getReceiptdate()));
            table.addCell(new Cell().add(billingList.get(i).getRremark()));
            table.addCell(new Cell().add(billingList.get(i).getBillamt() + ""));
            table.addCell(new Cell().add(billingList.get(i).getBillbalance() + ""));
        }
        document.add(table);

        document.close();
        Toast.makeText(this, "PDF Created!", Toast.LENGTH_SHORT).show();
    }

    public static ArrayList<BillingOfflineModel> billingList;

    @SuppressLint("Range")
    private void saveToList() {
        billingList = new ArrayList<>();
        Cursor cursor;
        switch (filter){
            case "date":
                cursor = helper.getAllBillsByDate();
                break;
            case "area":
                cursor = helper.getAllBillsByArea();
                break;
            case "party":
                cursor = helper.getAllBillsByParty();
                break;
            case "person":
                cursor = helper.getAllBillsByPerson();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + filter);
        }
        if (cursor != null) {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_BILL_ID));
                String party = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_PARTY));
                String partynm = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_PARTY_NM));
                String ref_name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_REF_NAME));
                String receipt_date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_RECEIPT_DATE));
                String receipt_no = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_RECEIPT_NO));
                String rbook_code = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_RBOOK_CODE));
                String rbook_nm = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_RBOOK_NM));
                String rp_type = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_RP_TYPE));
                float receipt_amt = cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.COL_RECEIPT_AMT));
                float bill_balance = cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.COL_BILL_BALANCE));
                int against_id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_AGAINST_ID));
                String rremark = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_RREMARK));
                String bbookcode = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_BBOOK_CODE));
                String bbooknm = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_BBOOK_NM));
                String billdate = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_BILL_DATE));
                String billno = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_BILL_NO));
                float billamt = cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.COL_BILL_AMT));
                int bidno = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_BID_NO));
                int btype = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_BTYPE));
                float adjustamount = cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.COL_ADJUST_AMOUNT));
                int bmark = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_BMARK));
                int days = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_DAYS));
                String billheader = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_BILL_HEADER));
                int crank = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_CRANK));
                String area = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_AREA));
                int arank = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_ARANK));
                String person = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_PERSON));
                int prank = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_PRANK));
                String frdate = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_FR_DATE));
                String todate = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_TO_DATE));
                int directpay = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_DIRECT_PAY));
                String chqdate = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_CHQ_DATE));
                int rmark = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_RMARK));
                String mobile = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_MOBILE));
                String bremark = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_BREMARK));

                BillingOfflineModel dataBaseModel = new BillingOfflineModel();
                dataBaseModel.setId(id);
                dataBaseModel.setParty(party);
                dataBaseModel.setPartynm(partynm);
                dataBaseModel.setRefname(ref_name);
                dataBaseModel.setReceiptdate(receipt_date);
                dataBaseModel.setReceiptno(receipt_no);
                dataBaseModel.setRbookcode(rbook_code);
                dataBaseModel.setRbooknm(rbook_nm);
                dataBaseModel.setRptype(rp_type);
                dataBaseModel.setReceiptamt(receipt_amt);
                dataBaseModel.setBillbalance(bill_balance);
                dataBaseModel.setAgainstid(against_id);
                dataBaseModel.setRremark(rremark);
                dataBaseModel.setBbookcode(bbookcode);
                dataBaseModel.setBbooknm(bbooknm);
                dataBaseModel.setBilldate(billdate);
                dataBaseModel.setBillno(billno);
                dataBaseModel.setBillamt(billamt);
                dataBaseModel.setBidno(bidno);
                dataBaseModel.setBtype(btype);
                dataBaseModel.setAdjustamount(adjustamount);
                dataBaseModel.setBmark(bmark);
                dataBaseModel.setDays(days);
                dataBaseModel.setBillheader(billheader);
                dataBaseModel.setCrank(crank);
                dataBaseModel.setArea(area);
                dataBaseModel.setArank(arank);
                dataBaseModel.setPerson(person);
                dataBaseModel.setPrank(prank);
                dataBaseModel.setFrdate(frdate);
                dataBaseModel.setTodate(todate);
                dataBaseModel.setDirectpay(directpay);
                dataBaseModel.setChqdate(chqdate);
                dataBaseModel.setRmark(rmark);
                dataBaseModel.setMobile(mobile);
                dataBaseModel.setBremark(bremark);
                billingList.add(dataBaseModel);
            }
            while (cursor.moveToNext());
            cursor.close();
        }
//        try {
//            createPDF();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
        Log.e("ToatalData", " : " + "Total data (Offline) : " + billingList.size());
    }


    private void generatePdf() throws FileNotFoundException {
        String path = new File(getExternalFilesDir(null).getPath() + "/" + getString(R.string.app_name)).getPath();
        Log.d("TAG", "generatePdf: " + "Folder created");
        File file = new File(path);
        File pdfPath = new File(file, "DemoPDF.pdf");
        if (!file.exists())
            if (file.mkdir())
                Log.d("TAG", "generatePdf: " + "Folder created");
            else Log.d("TAG", "generatePdf: " + "Error creating Folder");
        else Log.d("TAG", "generatePdf: " + "Folder exists");

        PdfWriter pdfWriter = new PdfWriter(pdfPath.getPath());
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);

        float[] columnWidth = {200f, 300f, 900f};
        Table table = new Table(columnWidth);

        Cell cell_11 = new Cell();
        cell_11.add("Item");
        table.addCell(cell_11);

        table.addCell(new Cell().add("Qty"));
        table.addCell(new Cell().add("Available"));

        table.addCell(new Cell().add("Mango"));
        table.addCell(new Cell().add("2 Kg"));
        table.addCell(new Cell().add("Yes"));

        table.addCell(new Cell().add("Apple"));
        table.addCell(new Cell().add("1 Kg"));
        table.addCell(new Cell().add("No"));

        table.addCell(new Cell().add("Banana"));
        table.addCell(new Cell().add("5 Kg"));
        table.addCell(new Cell().add("Yes"));

        document.add(table);

        document.close();
        Toast.makeText(this, "PDF Created!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 123) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                try {
//                    createPDF();
//                } catch (FileNotFoundException e) {
//                    Log.d(TAG, "onRequestPermissionsResult: " + e.getMessage());
//                }
            } else PermissionUtils.requestStoragePermission(BillActivity.this, 123);
        }
    }
}