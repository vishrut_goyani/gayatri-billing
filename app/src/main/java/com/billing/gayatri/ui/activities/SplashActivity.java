package com.billing.gayatri.ui.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.billing.gayatri.GayatriApp;
import com.billing.gayatri.R;
import com.billing.gayatri.apihelpers.RetrofitClient;
import com.billing.gayatri.models.LoginPost;
import com.billing.gayatri.models.LoginResponse;
import com.billing.gayatri.services.UpdateService;
import com.billing.gayatri.utils.NetworkState;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    Context context;
    public UpdateService updateService;
    boolean serviceBound = false;
    public static final String ACTION_DATA_SAVED = "com.bililng.gayatri.ACTION_DATA_SAVED";

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            updateService = ((UpdateService.LocalBinder) iBinder).getService();
            serviceBound = true;
        }

        public void onServiceDisconnected(ComponentName componentName) {
            serviceBound = false;
        }
    };

    BroadcastReceiver saved_receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            startActivity(new Intent(context, HomeFilterActivity.class));
            finish();
        }
    };

    ProgressDialog progressDialogOffline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        registerReceiver(saved_receiver, new IntentFilter(ACTION_DATA_SAVED));
        if (!serviceBound)
            startDownloadingService();
        if (NetworkState.isOnline()) {
            Toast.makeText(context, "Logging in", Toast.LENGTH_SHORT).show();
            loginUser();
        }
    }

    private void startDownloadingService() {
        Intent intent = new Intent(SplashActivity.this, UpdateService.class);
        intent.setAction(UpdateService.ACTION_DOWNLOADING_STARTED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startForegroundService(intent);
        else startService(intent);
        bindService(intent, serviceConnection, BIND_AUTO_CREATE);
    }

    private void loginUser() {
        LoginPost loginPost = new LoginPost("Test");
        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().loginUser(loginPost);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.code() == 200) {
                    GayatriApp.mmkv.putString("auth_token", response.body().getResponseData().getToken());
                    Log.e("TAG", "onResponse: " + GayatriApp.mmkv.decodeString("auth_token"));

                    Log.e("TAG", "onResponseBound: " + serviceBound);
                    if (serviceBound)
                        sendBroadcast(new Intent(UpdateService.ACTION_DOWNLOADING_STARTED));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(context, "Login failed.", Toast.LENGTH_SHORT).show();
                Log.e("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    private void initProgress() {
        progressDialogOffline = new ProgressDialog(SplashActivity.this);
        progressDialogOffline.setCancelable(false);
        progressDialogOffline.setMessage("Saving offline...");
    }

    private void showProgress() {
        progressDialogOffline.show();
    }

    private void hideProgress() {
        progressDialogOffline.dismiss();
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(saved_receiver);
        super.onDestroy();
    }
}