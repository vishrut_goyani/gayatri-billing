package com.billing.gayatri.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.billing.gayatri.R;
import com.billing.gayatri.ui.activities.BillActivity;
import com.google.android.material.card.MaterialCardView;

public class HomeFilterActivity extends AppCompatActivity {
    MaterialCardView cardDate, cardArea, cardParty, cardPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_filter);

        initIds();

        cardDate.setOnClickListener(v->{
            startActivity(new Intent(this, BillActivity.class)
            .putExtra("filter", "date"));
        });

        cardArea.setOnClickListener(v->{
            startActivity(new Intent(this, BillActivity.class)
                    .putExtra("filter", "area"));
        });

        cardParty.setOnClickListener(v->{
            startActivity(new Intent(this, BillActivity.class)
                    .putExtra("filter", "party"));
        });

        cardPerson.setOnClickListener(v->{
            startActivity(new Intent(this, BillActivity.class)
                    .putExtra("filter", "person"));
        });
    }

    private void initIds() {
        cardDate = findViewById(R.id.cardDate);
        cardArea = findViewById(R.id.cardArea);
        cardParty = findViewById(R.id.cardParty);
        cardPerson = findViewById(R.id.cardPerson);
    }
}