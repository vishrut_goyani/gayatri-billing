package com.billing.gayatri.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import com.billing.gayatri.GayatriApp;

public class NetworkState {
    public static boolean isOnline() {
        ConnectivityManager connectivityManager;
        if (!(GayatriApp.getInstance() == null || (connectivityManager = (ConnectivityManager) GayatriApp.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE)) == null)) {
            if (Build.VERSION.SDK_INT >= 29) {
                NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
                if (networkCapabilities != null) {
                    if (!networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) && !networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET);
                    }
                    return true;
                }
            } else {
                try {
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                        return false;
                    }
                    return true;
                } catch (Exception unused) {
                }
            }
        }
        return false;
    }
}
