package com.billing.gayatri.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "GayatriBilling";
    public static final String BILLING_TABLE = "billing_table";

    public static final String COL_ID = "COL_ID";
    public static final String COL_BILL_ID = "COL_BILL_ID";
    public static final String COL_PARTY = "COL_PARTY";
    public static final String COL_PARTY_NM = "COL_PARTY_NM";
    public static final String COL_REF_NAME = "COL_REF_NAME";
    public static final String COL_RECEIPT_DATE = "COL_RECEIPT_DATE";
    public static final String COL_RECEIPT_NO = "COL_RECEIPT_NO";
    public static final String COL_RBOOK_CODE = "COL_RBOOK_CODE";
    public static final String COL_RBOOK_NM = "COL_RBOOK_NM";
    public static final String COL_RP_TYPE = "COL_RP_TYPE";
    public static final String COL_RECEIPT_AMT = "COL_RECEIPT_AMT";
    public static final String COL_BILL_BALANCE = "COL_BILL_BALANCE";
    public static final String COL_AGAINST_ID = "COL_AGAINST_ID";
    public static final String COL_RREMARK = "COL_RREMARK";
    public static final String COL_BBOOK_CODE = "COL_BOOK_CODE";
    public static final String COL_BBOOK_NM = "COL_BBOOK_NM";
    public static final String COL_BILL_DATE = "COL_BILL_DATE";
    public static final String COL_BILL_NO = "COL_BILL_NO";
    public static final String COL_BILL_AMT = "COL_BILL_AMT";
    public static final String COL_BID_NO = "COL_BID_NO";
    public static final String COL_BTYPE = "COL_BTYPE";
    public static final String COL_ADJUST_AMOUNT = "COL_ADJUST_AMOUNT";
    public static final String COL_BMARK = "COL_BMARK";
    public static final String COL_DAYS = "COL_DAYS";
    public static final String COL_BILL_HEADER = "COL_BILL_HEADER";
    public static final String COL_CRANK = "COL_BILL_CRANK";
    public static final String COL_AREA = "COL_AREA";
    public static final String COL_ARANK = "COL_ARANK";
    public static final String COL_PERSON = "COL_PERSON";
    public static final String COL_PRANK = "COL_PRANK";
    public static final String COL_FR_DATE = "COL_FR_DATE";
    public static final String COL_TO_DATE = "COL_TO_DATE";
    public static final String COL_DIRECT_PAY = "COL_DIRECT_PAY";
    public static final String COL_CHQ_DATE = "COL_CHQ_DATE";
    public static final String COL_RMARK = "COL_RMARK";
    public static final String COL_MOBILE = "COL_MOBILE";
    public static final String COL_BREMARK = "COL_BREMARK";

    public static final String[] billColumns = {
            COL_ID,
            COL_BILL_ID,
            COL_PARTY,
            COL_PARTY_NM,
            COL_REF_NAME,
            COL_RECEIPT_DATE,
            COL_RECEIPT_NO,
            COL_RBOOK_CODE,
            COL_RBOOK_NM,
            COL_RP_TYPE,
            COL_RECEIPT_AMT,
            COL_BILL_BALANCE,
            COL_AGAINST_ID,
            COL_RREMARK,
            COL_BBOOK_CODE,
            COL_BBOOK_NM,
            COL_BILL_DATE,
            COL_BILL_NO,
            COL_BILL_AMT,
            COL_BID_NO,
            COL_BTYPE,
            COL_ADJUST_AMOUNT,
            COL_BMARK,
            COL_DAYS,
            COL_BILL_HEADER,
            COL_CRANK,
            COL_AREA,
            COL_ARANK,
            COL_PERSON,
            COL_PRANK,
            COL_FR_DATE,
            COL_TO_DATE,
            COL_DIRECT_PAY,
            COL_CHQ_DATE,
            COL_RMARK,
            COL_MOBILE,
            COL_BREMARK
    };

    private static final String CREATE_TABLE_BILL =
            "CREATE TABLE IF NOT EXISTS " + BILLING_TABLE
                    + "(" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COL_BILL_ID + " INTEGER,"
                    + COL_PARTY + " TEXT,"
                    + COL_PARTY_NM + " TEXT,"
                    + COL_REF_NAME + " TEXT,"
                    + COL_RECEIPT_DATE + " TEXT,"
                    + COL_RECEIPT_NO + " TEXT,"
                    + COL_RBOOK_CODE + " TEXT,"
                    + COL_RBOOK_NM + " TEXT,"
                    + COL_RP_TYPE + " TEXT,"
                    + COL_RECEIPT_AMT + " FLOAT,"
                    + COL_BILL_BALANCE + " FLOAT,"
                    + COL_AGAINST_ID + " INTEGER,"
                    + COL_RREMARK + " TEXT,"
                    + COL_BBOOK_CODE + " TEXT,"
                    + COL_BBOOK_NM + " TEXT,"
                    + COL_BILL_DATE + " TEXT,"
                    + COL_BILL_NO + " TEXT,"
                    + COL_BILL_AMT + " FLOAT,"
                    + COL_BID_NO + " INTEGER,"
                    + COL_BTYPE + " INTEGER,"
                    + COL_ADJUST_AMOUNT + " FLOAT,"
                    + COL_BMARK + " INTEGER,"
                    + COL_DAYS + " INTEGER,"
                    + COL_BILL_HEADER + " TEXT,"
                    + COL_CRANK + " INTEGER,"
                    + COL_AREA + " TEXT,"
                    + COL_ARANK + " INTEGER,"
                    + COL_PERSON + " TEXT,"
                    + COL_PRANK + " INTEGER,"
                    + COL_FR_DATE + " TEXT,"
                    + COL_TO_DATE + " TEXT,"
                    + COL_DIRECT_PAY + " INTEGER,"
                    + COL_CHQ_DATE + " TEXT,"
                    + COL_RMARK + " INTEGER,"
                    + COL_MOBILE + " TEXT,"
                    + COL_BREMARK + " TEXT,"
                    + "UNIQUE (" + COL_BILL_ID + ")" + ")";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_BILL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + BILLING_TABLE);
        onCreate(sqLiteDatabase);
    }

    public Cursor getAllBills() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM " + BILLING_TABLE, null);
        return res;
    }

    public Cursor getAllBillsByDate() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.query(BILLING_TABLE, null, null, null, null, null,
                COL_BILL_DATE + " DESC, "
                        + COL_PRANK + " DESC, "
                        + COL_ARANK + " DESC, "
                        + COL_CRANK + " DESC, "
                        + COL_FR_DATE + " DESC, "
                        + COL_TO_DATE + " DESC, "
                        + COL_BILL_DATE + " DESC"
        );
        return res;
    }

    public Cursor getAllBillsByArea() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.query(BILLING_TABLE, null, null, null, null, null,
                COL_AREA + " DESC, "
                        + COL_PRANK + " DESC, "
                        + COL_ARANK + " DESC, "
                        + COL_CRANK + " DESC, "
                        + COL_FR_DATE + " DESC, "
                        + COL_TO_DATE + " DESC, "
                        + COL_BILL_DATE + " DESC");
        return res;
    }

    public Cursor getAllBillsByParty() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.query(BILLING_TABLE, null, null, null, null, null,
                COL_PARTY + " DESC, "
                        + COL_PRANK + " DESC, "
                        + COL_ARANK + " DESC, "
                        + COL_CRANK + " DESC, "
                        + COL_FR_DATE + " DESC, "
                        + COL_TO_DATE + " DESC, "
                        + COL_BILL_DATE + " DESC");
        return res;
    }

    public Cursor getAllBillsByPerson() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.query(BILLING_TABLE, null, null, null, null, null,
                COL_PERSON + " DESC, "
                        + COL_PRANK + " DESC, "
                        + COL_ARANK + " DESC, "
                        + COL_CRANK + " DESC, "
                        + COL_FR_DATE + " DESC, "
                        + COL_TO_DATE + " DESC, "
                        + COL_BILL_DATE + " DESC");
        return res;
    }

    public Cursor getSearchedPartyName(String partyName) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery(" SELECT * FROM " + BILLING_TABLE +
                " WHERE " + COL_PARTY + " LIKE '" + partyName + "%'", null);
        return res;
    }
}
