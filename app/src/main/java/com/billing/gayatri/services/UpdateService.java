package com.billing.gayatri.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.billing.gayatri.GayatriApp;
import com.billing.gayatri.alarm_manager.Alarm;
import com.billing.gayatri.alarm_manager.BuildNotificationUtils;
import com.billing.gayatri.apihelpers.RetrofitClient;
import com.billing.gayatri.database.DatabaseHelper;
import com.billing.gayatri.models.BillingModel;
import com.billing.gayatri.ui.activities.SplashActivity;
import com.billing.gayatri.utils.NetworkState;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateService extends Service {
    public static final String CHANNEL_ID = "Bill channel id";
    public static final CharSequence CHANNEL_NAME = "Bill channel name";
    public static final String ACTION_DOWNLOADING_STARTED = "com.bililng.gayatri.ACTION_DOWNLOADING_STARTED";
    private static final String ACTION_DOWNLOADING_COMPLETED = "com.bililng.gayatri.ACTION_DOWNLOADING_COMPLETED";
    private final IBinder binder = new LocalBinder();
    Alarm alarm = new Alarm();

    public enum DOWNLOADING {
        STARTED,
        COMPLETED
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public UpdateService getService() {
            return UpdateService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    ArrayList<BillingModel.BillingData> allBillingData;
    SQLiteDatabase database;
    DatabaseHelper helper;

    @Override
    public void onCreate() {
        super.onCreate();
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        helper = databaseHelper;
        database = databaseHelper.getWritableDatabase();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIncomingActions(intent);
        return START_STICKY;
    }

    private void handleIncomingActions(Intent intent) {
        Notification notification = null;
        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();
            if (action.equalsIgnoreCase(ACTION_DOWNLOADING_STARTED)) {
                if (helper.getAllBills().getCount() > 0) {
                    sendBroadcast(new Intent(SplashActivity.ACTION_DATA_SAVED));
                    stopForeground(true);
                    stopSelf();
                } else startDownloading();
                if (BuildNotificationUtils.buildForegroundNotification(this, DOWNLOADING.STARTED) instanceof NotificationCompat.Builder)
                    notification = ((NotificationCompat.Builder) BuildNotificationUtils.buildForegroundNotification(this, DOWNLOADING.STARTED)).build();
                else
                    notification = ((Notification.Builder) BuildNotificationUtils.buildForegroundNotification(this, DOWNLOADING.STARTED)).build();
//                alarm.setAlarm(this);
            } else if (action.equalsIgnoreCase(ACTION_DOWNLOADING_COMPLETED)) {
                if (BuildNotificationUtils.buildForegroundNotification(this, DOWNLOADING.COMPLETED) instanceof NotificationCompat.Builder)
                    notification = ((NotificationCompat.Builder) BuildNotificationUtils.buildForegroundNotification(this, DOWNLOADING.COMPLETED)).build();
                else
                    notification = ((Notification.Builder) BuildNotificationUtils.buildForegroundNotification(this, DOWNLOADING.COMPLETED)).build();
//                alarm.cancelAlarm(this);
                sendBroadcast(new Intent(SplashActivity.ACTION_DATA_SAVED));
            }
            if (notification != null)
                startForeground(1, notification);
            Log.e("TAG", "handleIncomingActions: " + notification);
        }
    }

    private void startDownloading() {
        if (NetworkState.isOnline()) {
            loadBillingData();
        } else {
            stopSelf();
        }
    }

    private void loadBillingData() {
        allBillingData = new ArrayList<>();
        RetrofitClient.getInstance().getApi().getAllBillingData("Bearer " + GayatriApp.mmkv.decodeString("auth_token")).enqueue(new Callback<BillingModel>() {
            @Override
            public void onResponse(Call<BillingModel> call, Response<BillingModel> response) {
                Log.e("TAG", "onResponse: " + response.body().getResponseData().get(0).getId());
                Log.e("ToatalData", " : " + "Total data (Server) : " + allBillingData.size());
                if (response.body() != null && response.body().isSuccess()) {
                    try {
                        allBillingData = response.body().getResponseData();
                        if (allBillingData.size() > 0) {
                            downloadData(allBillingData);
                        }
                    } catch (Exception e) {
                        Log.e("TAG", "onError: " + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<BillingModel> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    private void downloadData(ArrayList<BillingModel.BillingData> billingList) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Handler handler = new Handler(Looper.getMainLooper());

        executor.execute(() -> {
            //Background work here
            doInBackground(billingList);

            //Do UI Thread work here
            handler.post(() -> {
                sendBroadcast(new Intent(SplashActivity.ACTION_DATA_SAVED));
                stopForeground(true);
                stopSelf();
            });
        });
    }

    private void doInBackground(ArrayList<BillingModel.BillingData> arrayList) {
        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        for (int i = 0; i < arrayList.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseHelper.COL_BILL_ID, arrayList.get(i).getId());
            contentValues.put(DatabaseHelper.COL_PARTY, arrayList.get(i).getParty());
            contentValues.put(DatabaseHelper.COL_PARTY_NM, arrayList.get(i).getPartynm());
            contentValues.put(DatabaseHelper.COL_REF_NAME, arrayList.get(i).getRefname());
            contentValues.put(DatabaseHelper.COL_RECEIPT_DATE, arrayList.get(i).getReceiptdate());
            contentValues.put(DatabaseHelper.COL_RECEIPT_NO, arrayList.get(i).getReceiptno());
            contentValues.put(DatabaseHelper.COL_RBOOK_CODE, arrayList.get(i).getRbookcode());
            contentValues.put(DatabaseHelper.COL_RBOOK_NM, arrayList.get(i).getRbooknm());
            contentValues.put(DatabaseHelper.COL_RP_TYPE, arrayList.get(i).getRptype());
            contentValues.put(DatabaseHelper.COL_RECEIPT_AMT, arrayList.get(i).getReceiptamt());
            contentValues.put(DatabaseHelper.COL_BILL_BALANCE, arrayList.get(i).getBillbalance());
            contentValues.put(DatabaseHelper.COL_AGAINST_ID, arrayList.get(i).getAgainstid());
            contentValues.put(DatabaseHelper.COL_RREMARK, arrayList.get(i).getRremark());
            contentValues.put(DatabaseHelper.COL_BBOOK_CODE, arrayList.get(i).getBbookcode());
            contentValues.put(DatabaseHelper.COL_BBOOK_NM, arrayList.get(i).getBbooknm());
            contentValues.put(DatabaseHelper.COL_BILL_DATE, arrayList.get(i).getBilldate());
            contentValues.put(DatabaseHelper.COL_BILL_NO, arrayList.get(i).getBillno());
            contentValues.put(DatabaseHelper.COL_BILL_AMT, arrayList.get(i).getBillamt());
            contentValues.put(DatabaseHelper.COL_BID_NO, arrayList.get(i).getBidno());
            contentValues.put(DatabaseHelper.COL_BTYPE, arrayList.get(i).getBtype());
            contentValues.put(DatabaseHelper.COL_ADJUST_AMOUNT, arrayList.get(i).getAdjustamount());
            contentValues.put(DatabaseHelper.COL_BMARK, arrayList.get(i).getBmark());
            contentValues.put(DatabaseHelper.COL_DAYS, arrayList.get(i).getDays());
            contentValues.put(DatabaseHelper.COL_BILL_HEADER, arrayList.get(i).getBillheader());
            contentValues.put(DatabaseHelper.COL_CRANK, arrayList.get(i).getCrank());
            contentValues.put(DatabaseHelper.COL_AREA, arrayList.get(i).getArea());
            contentValues.put(DatabaseHelper.COL_ARANK, arrayList.get(i).getArank());
            contentValues.put(DatabaseHelper.COL_PERSON, arrayList.get(i).getPerson());
            contentValues.put(DatabaseHelper.COL_PRANK, arrayList.get(i).getPrank());
            contentValues.put(DatabaseHelper.COL_FR_DATE, arrayList.get(i).getFrdate());
            contentValues.put(DatabaseHelper.COL_TO_DATE, arrayList.get(i).getTodate());
            contentValues.put(DatabaseHelper.COL_DIRECT_PAY, arrayList.get(i).getDirectpay());
            contentValues.put(DatabaseHelper.COL_CHQ_DATE, arrayList.get(i).getChqdate());
            contentValues.put(DatabaseHelper.COL_RMARK, arrayList.get(i).getRmark());
            contentValues.put(DatabaseHelper.COL_MOBILE, arrayList.get(i).getMobile());
            contentValues.put(DatabaseHelper.COL_BREMARK, arrayList.get(i).getBremark());
            db.insert(DatabaseHelper.BILLING_TABLE, null, contentValues);
        }
        db.setTransactionSuccessful();
        db.endTransaction();
    }
}
