package com.billing.gayatri;

import androidx.multidex.MultiDexApplication;

import com.tencent.mmkv.MMKV;

public class GayatriApp extends MultiDexApplication {
    static GayatriApp mInstance;
    public static MMKV mmkv;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        MMKV.initialize(this);
        mmkv = MMKV.defaultMMKV();
    }

    public static synchronized GayatriApp getInstance() {
        return mInstance;
    }
}
