package com.billing.gayatri.alarm_manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.billing.gayatri.services.UpdateService;

public class AutoStart extends BroadcastReceiver {
    Alarm alarm = new Alarm();

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_LOCKED_BOOT_COMPLETED)) {
            Intent serviceIntent = new Intent(context, UpdateService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(serviceIntent);
            } else {
                context.startService(serviceIntent);
            }
            this.alarm.setAlarm(context);
        }
    }
}
