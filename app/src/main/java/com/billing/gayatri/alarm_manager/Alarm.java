package com.billing.gayatri.alarm_manager;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.PowerManager;

import com.billing.gayatri.services.UpdateService;

public class Alarm extends BroadcastReceiver {
    public static final int ALARM_REQUEST_CODE = 123456;

    public void onReceive(Context context, Intent intent) {
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getSystemService(Context.POWER_SERVICE)).newWakeLock(1, ":ClockUpdateService");
        newWakeLock.acquire(600000);
        context.getPackageManager().setComponentEnabledSetting(new ComponentName(context, UpdateService.class),
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);


        newWakeLock.release();
    }


    public void setAlarm(Context context) {
        try {
            setExactIdleAlarm(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setExactIdleAlarm(Context context) {
        Intent intent = new Intent(context, Alarm.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(0, broadcast), broadcast);

    }

    public void cancelAlarm(Context context) {
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE)).cancel(PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, new Intent(context, Alarm.class), 0));
    }
}
