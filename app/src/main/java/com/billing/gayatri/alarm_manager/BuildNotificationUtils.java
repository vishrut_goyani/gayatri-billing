package com.billing.gayatri.alarm_manager;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.billing.gayatri.R;
import com.billing.gayatri.services.UpdateService;
import com.billing.gayatri.ui.activities.BillActivity;

public class BuildNotificationUtils {
    public static Object buildForegroundNotification(Context context, UpdateService.DOWNLOADING downloadingStatus) {
        String downloadingStr;
        Resources resources = context.getResources();
        Notification.Builder builder = null;
        NotificationCompat.Builder builderCompat = null;
        int smallIcon = R.drawable.bill_notification_icon;
        Bitmap largeIcon = BitmapFactory.decodeResource(resources, R.drawable.app_icon);
        if (downloadingStatus == UpdateService.DOWNLOADING.STARTED) {
            downloadingStr = "Updating bills data...";
        } else {
            downloadingStr = "Bills data updated.";
        }
        PendingIntent activity = PendingIntent.getActivity(context, 0, new Intent(context, BillActivity.class), 0);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(UpdateService.CHANNEL_ID, UpdateService.CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context,
                    UpdateService.CHANNEL_ID)
                    .setShowWhen(false)
                    .setOnlyAlertOnce(true)
                    .setOngoing(true)
                    .setPriority(1)
                    .setColor(context.getColor(R.color.colorPrimary))
                    .setLargeIcon(largeIcon)
                    .setSmallIcon(smallIcon)
                    .setContentText(downloadingStr)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentIntent(activity);
            builderCompat = notificationBuilder;
        } else {
            Notification.Builder notificationBuilder = new Notification.Builder(context)
                    .setShowWhen(false)
                    .setOnlyAlertOnce(true)
                    .setOngoing(true)
                    .setPriority(Notification.PRIORITY_DEFAULT)
                    .setColor(context.getResources().getColor(R.color.colorPrimary))
                    .setLargeIcon(largeIcon)
                    .setSmallIcon(smallIcon)
                    .setContentText(downloadingStr)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentIntent(activity);
            builder = notificationBuilder;
        }
        return builderCompat != null ? builderCompat : builder;
    }
}
