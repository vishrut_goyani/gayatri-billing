package com.billing.gayatri.models;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BillingOfflineModel {
    @SerializedName("adjustamount")
    float adjustamount;

    @SerializedName("againstid")
    int againstid;

    @SerializedName("arank")
    int arank;

    @SerializedName("area")
    String area;

    @SerializedName("bbookcode")
    String bbookcode;

    @SerializedName("bbooknm")
    String bbooknm;

    @SerializedName("bidno")
    int bidno;

    @SerializedName("billamt")
    float billamt;

    @SerializedName("billbalance")
    float billbalance;

    @SerializedName("billdate")
    String billdate;

    @SerializedName("billheader")
    String billheader;

    @SerializedName("billno")
    String billno;

    @SerializedName("bmark")
    int bmark;

    @SerializedName("bremark")
    String bremark;

    @SerializedName("btype")
    int btype;

    @SerializedName("chqdate")
    String chqdate;

    @SerializedName("crank")
    int crank;

    @SerializedName("days")
    int days;

    @SerializedName("directpay")
    int directpay;

    @SerializedName("frdate")
    String frdate;

    @SerializedName("id")
    int id;

    @SerializedName("mobile")
    String mobile;

    @SerializedName("party")
    String party;

    @SerializedName("partynm")
    String partynm;

    @SerializedName("person")
    String person;

    @SerializedName("prank")
    int prank;

    @SerializedName("rbookcode")
    String rbookcode;

    @SerializedName("rbooknm")
    String rbooknm;

    @SerializedName("receiptamt")
    float receiptamt;

    @SerializedName("receiptdate")
    String receiptdate;

    @SerializedName("receiptno")
    String receiptno;

    @SerializedName("refname")
    String refname;

    @SerializedName("rmark")
    int rmark;

    @SerializedName("rptype")
    String rptype;

    @SerializedName("rremark")
    String rremark;

    @SerializedName("todate")
    String todate;
}
