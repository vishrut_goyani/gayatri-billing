package com.billing.gayatri.models;


import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class LoginResponse {
    @SerializedName("isSuccess")
    boolean isSuccess;

    @SerializedName("responseData")
    ResponseData responseData;

    @SerializedName("errorCode")
    int errorCode;

    @SerializedName("errorMessage")
    String errorMessage;

    @Data
    public static class ResponseData {
        @SerializedName("rowid")
        int rowid;

        @SerializedName("person")
        String person;

        @SerializedName("token")
        String token;
    }
}
