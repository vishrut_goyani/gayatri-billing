package com.billing.gayatri.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BillingModel {
    @SerializedName("isSuccess")
    boolean isSuccess;

    @SerializedName("responseData")
    ArrayList<BillingData> responseData;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BillingData {
        @SerializedName("id")
        int id;

        @SerializedName("party")
        String party;

        @SerializedName("partynm")
        String partynm;

        @SerializedName("refname")
        String refname;

        @SerializedName("receiptdate")
        String receiptdate;

        @SerializedName("receiptno")
        String receiptno;

        @SerializedName("rbookcode")
        String rbookcode;

        @SerializedName("rbooknm")
        String rbooknm;

        @SerializedName("rptype")
        String rptype;

        @SerializedName("receiptamt")
        float receiptamt;

        @SerializedName("billbalance")
        float billbalance;

        @SerializedName("againstid")
        int againstid;

        @SerializedName("rremark")
        String rremark;

        @SerializedName("bbookcode")
        String bbookcode;

        @SerializedName("bbooknm")
        String bbooknm;

        @SerializedName("billdate")
        String billdate;

        @SerializedName("billno")
        String billno;

        @SerializedName("billamt")
        float billamt;

        @SerializedName("bidno")
        int bidno;

        @SerializedName("btype")
        int btype;

        @SerializedName("adjustamount")
        float adjustamount;

        @SerializedName("bmark")
        int bmark;

        @SerializedName("days")
        int days;

        @SerializedName("billheader")
        String billheader;

        @SerializedName("crank")
        int crank;

        @SerializedName("area")
        String area;

        @SerializedName("arank")
        int arank;

        @SerializedName("person")
        String person;

        @SerializedName("prank")
        int prank;

        @SerializedName("frdate")
        String frdate;

        @SerializedName("todate")
        String todate;

        @SerializedName("directpay")
        int directpay;

        @SerializedName("chqdate")
        String chqdate;

        @SerializedName("rmark")
        int rmark;

        @SerializedName("mobile")
        String mobile;

        @SerializedName("bremark")
        String bremark;
    }
}
