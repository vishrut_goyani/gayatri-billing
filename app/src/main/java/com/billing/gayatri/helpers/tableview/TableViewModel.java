package com.billing.gayatri.helpers.tableview;

import androidx.annotation.NonNull;

import com.billing.gayatri.helpers.tableview.models.Cell;
import com.billing.gayatri.ui.activities.BillActivity;

import java.util.ArrayList;
import java.util.List;

public class TableViewModel {

    // Columns indexes
    public static final int MOOD_COLUMN_INDEX = 3;
    public static final int GENDER_COLUMN_INDEX = 4;

    // Constant size for dummy data sets
    private static final int COLUMN_SIZE = 9;
    private static final int ROW_SIZE = BillActivity.billingList.size();

    @NonNull
    private List<RowHeader> getSimpleRowHeaderList() {
        List<RowHeader> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {

            RowHeader header = new RowHeader(String.valueOf(i), String.valueOf(i + 1));
            list.add(header);
        }

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
    @NonNull
    private List<ColumnHeader> getRandomColumnHeaderList() {
        List<ColumnHeader> list = new ArrayList<>();

        for (int i = 0; i < COLUMN_SIZE; i++) {
//            String title = "column " + i;
//            ColumnHeader header = new ColumnHeader(String.valueOf(i), title);
//            list.add(header);
        }

        ColumnHeader header = new ColumnHeader(String.valueOf(0), "Bill No.");
        list.add(header);
        header = new ColumnHeader(String.valueOf(1), "Days");
        list.add(header);
        header = new ColumnHeader(String.valueOf(2), "Bill Date");
        list.add(header);
        header = new ColumnHeader(String.valueOf(3), "Bill Amt");
        list.add(header);
        header = new ColumnHeader(String.valueOf(4), "Vou. No.");
        list.add(header);
        header = new ColumnHeader(String.valueOf(5), "Vou. Date.");
        list.add(header);
        header = new ColumnHeader(String.valueOf(6), "Note");
        list.add(header);
        header = new ColumnHeader(String.valueOf(7), "Vou. Amount");
        list.add(header);
        header = new ColumnHeader(String.valueOf(8), "Balance");
        list.add(header);

        return list;
    }

    /**
     * This is a dummy model list test some cases.
     */
//    @NonNull
//    private List<List<Cell>> getCellListForSortingTest() {
//        List<List<Cell>> list = new ArrayList<>();
//        for (int i = 0; i < ROW_SIZE; i++) {
//            List<Cell> cellList = new ArrayList<>();
//            for (int j = 0; j < COLUMN_SIZE; j++) {
//                Object text = "cell " + j + " " + i;
//
//                final int random = new Random().nextInt();
//                if (j == 0) {
//                    text = i;
//                } else if (j == 1) {
//                    text = random;
//                }
//
//                // Create dummy id.
//                String id = j + "-" + i;
//
//                Cell cell;
//                if (j == 3) {
//                    cell = new Cell(id, text);
//                } else if (j == 4) {
//                    // NOTE female and male keywords for filter will have conflict since "female"
//                    // contains "male"
//                    cell = new Cell(id, text);
//                } else {
//                    cell = new Cell(id, text);
//                }
//                cellList.add(cell);
//            }
//            list.add(cellList);
//        }
//
//        return list;
//    }
    @NonNull
    private List<List<Cell>> getCellListForSortingTest() {
        List<List<Cell>> list = new ArrayList<>();
        for (int i = 0; i < ROW_SIZE; i++) {
            List<Cell> cellList = new ArrayList<>();
            for (int j = 0; j < COLUMN_SIZE; j++) {
                Object text;
                switch (j) {
                    case 0:
                        text = BillActivity.billingList.get(i).getBillno();
                        break;
                    case 1:
                        text = BillActivity.billingList.get(i).getDays();
                        break;
                    case 2:
                        text = BillActivity.billingList.get(i).getBilldate();
                        break;
                    case 3:
                        text = BillActivity.billingList.get(i).getBillamt() + " /-";
                        break;
                    case 4:
                        text = BillActivity.billingList.get(i).getReceiptno();
                        break;
                    case 5:
                        text = BillActivity.billingList.get(i).getReceiptdate();
                        break;
                    case 6:
                        text = BillActivity.billingList.get(i).getRremark();
                        break;
                    case 7:
                        text = BillActivity.billingList.get(i).getReceiptamt() + " /-";
                        break;
                    case 8:
                        text = BillActivity.billingList.get(i).getBillbalance();
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + j);
                }

                // Create dummy id.
                String id = j + "-" + i;

                Cell cell;
                cell = new Cell(id, text);
                cellList.add(cell);
            }
            list.add(cellList);
        }

        return list;
    }

    @NonNull
    public List<List<Cell>> getCellList() {
        if (BillActivity.billingList.size() > 0)
            return getCellListForSortingTest();
        else return new ArrayList<>();
    }

    @NonNull
    public List<RowHeader> getRowHeaderList() {
        return getSimpleRowHeaderList();
    }

    @NonNull
    public List<ColumnHeader> getColumnHeaderList() {
        return getRandomColumnHeaderList();
    }
}
