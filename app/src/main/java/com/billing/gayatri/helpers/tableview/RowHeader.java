package com.billing.gayatri.helpers.tableview;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.billing.gayatri.helpers.tableview.models.Cell;

public class RowHeader extends Cell {
    public RowHeader(@NonNull String id, @Nullable String data) {
        super(id, data);
    }
}