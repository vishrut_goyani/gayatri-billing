package com.billing.gayatri.interfaces;

import com.billing.gayatri.models.BillingModel;
import com.billing.gayatri.models.LoginPost;
import com.billing.gayatri.models.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface APIService {
    @POST("User/LoginAuthenticate")
    Call<LoginResponse> loginUser(@Body LoginPost loginPost);

    @GET("User/GetOutstandingReportSyncData")
    Call<BillingModel> getAllBillingData(@Header("Authorization") String str);
}
