package com.evrencoskun.tableview.listener;

public interface ScrollButtonListener {
    void onScrollToFirst();
    void onScrollToLast();
}
